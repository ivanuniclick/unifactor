import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  logged: boolean;

  constructor(public router: Router) { }

  ngOnInit(): void {
    const session = sessionStorage.getItem('user');
    this.logged = JSON.parse(session);
  }

  goToDashboard() {
    sessionStorage.setItem('user', JSON.stringify(true));
    this.router.navigate(['dashboard']);
  }
}
