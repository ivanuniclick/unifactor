import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  logged: boolean;

  constructor() { }

  ngOnInit(): void {
    const session = sessionStorage.getItem('user');
    this.logged = JSON.parse(session);
  }

}
