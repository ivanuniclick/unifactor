import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    {
        path: 'login',
        loadChildren: () => import('./components/login/login.module').then(mod => mod.LoginModule)
    },
    {
        path: '',
        loadChildren: () => import('./components/home/home.module').then(mod => mod.HomeModule)
    },
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [RouterModule]
})
export class AppRoutingModule { }
